import React, { Component } from 'react'
import FormSinhVien from './FormSinhVien'
import TableSinhVien from './TableSinhVien'

export default class QuanLiSinhVien extends Component {
  render() {
    return (
      <div className='container my-5'>
            <FormSinhVien/>
            <TableSinhVien/>        
      </div>
    )
  }
}

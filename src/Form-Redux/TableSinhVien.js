import React, { Component } from 'react'
import {connect} from 'react-redux'
import { SUA_SINH_VIEN, XOA_SINH_VIEN } from './redux/types/typesSinhVien';

class TableSinhVien extends Component {
    renderSinhVien = ()=>{
        const {sinhVienArr} = this.props;
        return sinhVienArr.map((sinhVien,i) => { 
            return (
                <tr key={i}>
                    <td>{sinhVien.maSV}</td>
                    <td>{sinhVien.hoTen}</td>
                    <td>{sinhVien.soDienThoai}</td>
                    <td>{sinhVien.email}</td>
                    <td>
                        <button onClick={() => { this.props.xoaSinhVien(sinhVien.maSV) }} className='btn btn-danger mr-1'>Xóa</button>
                        <button onClick={() => { this.handleEdit(sinhVien.maSV);
                        
                        }} className='btn btn-primary'>Sửa</button>
                    </td>
                   
                </tr>
            )
         })
    }
    handleEdit = (maSV) => {
        let index = this.props.sinhVienArr.findIndex(sv => sv.maSV === maSV);
        if(index !== -1){
            this.props.suaSinhVien(this.props.sinhVienArr[index])
        }
    }
  render() {
    return (
      <div>
        <table className='table'>
            <thead>
                <tr className='bg-success text-white'>
                    <th>Mã SV</th>
                    <th>Họ Tên</th>
                    <th>Số Điện Thoại</th>
                    <th>Email</th>
                    <th>Thao Tác</th>
                </tr>
            </thead>
            <tbody>
                {this.renderSinhVien()}
            </tbody>
        </table>
      </div>
    )
  }
}
const mapStatetoProps = (state) => { 
    return {
        sinhVienArr : state.sinhVienReducer.sinhVienArr,
    }
 }
 let mapDispatchToProps = (dispatch)=>{
    return {
        xoaSinhVien :(maSV)=>{
            dispatch ({
                type : XOA_SINH_VIEN,
                payload:maSV ,
            })
        },
        suaSinhVien :(sv)=>{
            dispatch ({
                type : SUA_SINH_VIEN,
                payload:sv ,
            })
        }
    }
}

export default connect(mapStatetoProps,mapDispatchToProps)(TableSinhVien)
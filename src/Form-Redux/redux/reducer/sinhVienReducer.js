import { initaiUser } from "../../utilSinhVien";
import { CAP_NHAT_SINH_VIEN, SUA_SINH_VIEN, THEM_SINH_VIEN, XOA_SINH_VIEN } from "../types/typesSinhVien";


let intialState = {
    sinhVienArr : [],
    sinhVienEdit: null,
}

export let sinhVienReducer = (state = intialState, action) => {
    switch (action.type) {
        case THEM_SINH_VIEN:{
            let index = state.sinhVienArr.findIndex(item => item.maSV === action.sinhVien.maSV)
            if(index == -1){
                const sinhVienUpdate = [...state.sinhVienArr,action.sinhVien]
                state.sinhVienArr = sinhVienUpdate
            }else{
                alert("Mã Sinh Viên trùng khớp. Vui lòng nhập lại!!!")
            }
            return {...state}
        }
        case XOA_SINH_VIEN :{
            const sinhVienClone = [...state.sinhVienArr]
            let index = state.sinhVienArr.findIndex(item => item.maSV === action.payload )
            sinhVienClone.splice(index,1)
            state.sinhVienArr = [...sinhVienClone]
            return{...state}

        }
        case SUA_SINH_VIEN:{
            const sinhVienUpdate =  action.payload
            state.sinhVienEdit = sinhVienUpdate;
            console.log(state)
            return {...state}
        }
        case CAP_NHAT_SINH_VIEN : {
            let index = state.sinhVienArr.findIndex(item => item.maSV === action.payload.maSV);
            if(index !== -1){
                let sinhVienUpdate = [...state.sinhVienArr]
                sinhVienUpdate[index] = action.payload
                state.sinhVienArr = sinhVienUpdate
                state.sinhVienEdit = null
                console.log(state)
                return {...state}
            }
        }
        default:{
            return  {...state};
        }
    }
}
import React, { Component } from "react";
import { connect } from 'react-redux';
import { CAP_NHAT_SINH_VIEN, CHANGE_VALUE, THEM_SINH_VIEN } from "./redux/types/typesSinhVien";
import { initaiUser } from "./utilSinhVien";
 class FormSinhVien extends Component {


    state = {
        value : {
            maSV : "",
            hoTen : "",
            soDienThoai : "",
            email: "",
        },
        error :{
            maSV : "",
            hoTen : "",
            soDienThoai : "",
            email: "",
        },
        valid : false

    }
    
    handleChange = (event) => {
        let tagInput = event.target;
        let {name,value,type,pattern} = tagInput

        let errorMessage = ''
        
        if(value.trim() === ''){
            errorMessage = name + " không được bỏ trống!"
        }
        if(type === 'email'  ){
            const regex = new RegExp(pattern)
            if ( !regex.test(value)){
                errorMessage = name + " không hợp lệ!";
            }
        }
        if(type === 'number'){
            const regex = new RegExp(pattern)
            if ( !regex.test(value)){
                errorMessage = name + " không hợp lệ!";
            }
        }
        let values = {...this.state.value,[name] : value}
        let errors = {...this.state.error,[name] : errorMessage}

        this.setState({
            ...this.state,
            value : values,
            error : errors,
        })
    }
    handleSubmit = (event) => { 
        event.preventDefault();
        this.props.themSinhVien(this.state.value)
        this.setState({
            value : initaiUser,
        })
     }
     checkValid = () => { 
        let valid = true;
        for (let key in this.state.error){
            if(this.state.error[key] !== '' && this.state.value[key] === ""){
                valid = false
            }
        }
       
        this.setState({
            ...this.state,
            valid : valid
        })
      }
     
      static getDerivedStateFromProps(props, state){
        if(!state.value.maSV && props.sinhVienEdit ){
            return {value : props.sinhVienEdit}
        }
      }
    render() {
    return (
      <div>
        <h4 className="p-2 bg-success text-white">Thông Tin Sinh Viên</h4>
        <form onSubmit={this.handleSubmit}>
            <div className="row">
                <div className="form-group col-6">
                    <span>Mã SV</span>
                    <input className="form-control" name="maSV" value={this.state.value.maSV} onChange={this.handleChange}/>
                    <p className="text-danger">{this.state.error.maSV}</p>
                </div>
                <div className="form-group col-6">
                    <span>Họ Tên</span>
                    <input className="form-control" name="hoTen"  value={this.state.value.hoTen} onChange={this.handleChange} />
                    <p className="text-danger">{this.state.error.hoTen}</p>
                </div>
            </div>
            <div className="row">
            <div className="form-group col-6">
                    <span>Số Điện Thoại</span>
                    <input pattern="(84|0[3|5|7|8|9])+([0-9]{8})\b" type="number" className="form-control" name="soDienThoai" value={this.state.value.soDienThoai} onChange={this.handleChange} />
                    <p className="text-danger">{this.state.error.soDienThoai}</p>
                </div>
                <div className="form-group col-6">
                    <span>Email</span>
                    <input pattern="^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$" type="email" className="form-control" name="email"  value={this.state.value.email} onChange={this.handleChange} />
                    <p className="text-danger">{this.state.error.email}</p>
                </div>
            </div>
            <div className="row mb-4">
              <div className="col-12">
                {this.checkValid 
                ? <button type="submit" className="btn btn-dark">Thêm Sinh Viên</button>  
                : <button type="submit" className="btn btn-dark" disabled>Thêm Sinh Viên</button>  }

                {this.props.sinhVienEdit ? 
                <button type="button" onClick={() => { this.props.capNhatSinhVien(this.state.value); this.setState({value : initaiUser}) }} className="btn btn-primary ml-3">Cập Nhật</button> :
                <button type="button" className="btn btn-primary d-none ">Cập Nhật</button> 
                }
              </div>      
            </div>
        </form>
      </div>
    );
  }
}
const mapStatetoProps = (state) => { 
    return {
        sinhVienEdit : state.sinhVienReducer.sinhVienEdit,
    }
 }
let mapDispatchToProps = (dispatch)=>{
    return {
        themSinhVien :(sinhVien)=>{
            dispatch ({
                type : THEM_SINH_VIEN,
                sinhVien ,
            })
        },
        capNhatSinhVien : (sinhVien) => {
            dispatch ({
                type: CAP_NHAT_SINH_VIEN,
                payload : sinhVien,
            })
        }
    }
}
export default connect(mapStatetoProps,mapDispatchToProps)(FormSinhVien)